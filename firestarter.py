from tkinter import *
from tkinter import font
from twisted.internet.protocol import ClientFactory, Protocol, connectionDone
from twisted.internet import reactor
import sys, socket, os, time, re, threading
import updater
import tkhyperlink, codepaste
import webbrowser
from datetime import datetime as dt

version = "1.3"
changes = ["Code pasting! Have a look in the menu bar."]


class Client(Protocol):
    def dataReceived(self, line):
        rawmsg = line.decode()
        if rawmsg == "IF YOU CAN SEE THIS MESSAGE, YOU NEED TO UPDATE.\nhttp://bitbucket.com/alicejenny/firestarter/downloads\r\n":
            self.sendMsg("/silent Alice: {}".format(version))
        elif re.search("\[BEGIN CODE\](.+)", rawmsg):
            code = re.search("\[BEGIN CODE\](.+)", rawmsg, re.DOTALL).groups()[0]
            print(code)
            action = lambda: codepaste.CodeView(root.bg, root.fontcol, root.fontface, root.icon, code, root.f, root)
            user = re.search("\[#[A-Za-z0-9]{,6}\](.{1,20}):", rawmsg).groups()[0]
            root.chatbox.config(state = NORMAL)
            root.chatbox.insert(END, "View Code from " + user, root.hyperlinkmanager.add(action))
            root.chatbox.insert(END, "\r\n")
            root.chatbox.config(state = DISABLED)
        else:
            try:
                colour = re.search("\[(#[A-Za-z0-9]{,6})\]", rawmsg).groups()[0]
                root.chatbox.tag_config("colour-{}".format(colour), foreground = colour)
                msg = re.sub("\[(#[A-Za-z0-9]{,6})\]", "", rawmsg, 1)
            except:
                msg = rawmsg
                colour = None

            root.chatbox.config(state = NORMAL)
            if root.showcolours and colour:
                if re.search("https?:.+", msg):
                    self.hyperlink(msg, colour)
                else:
                    root.chatbox.insert(END, msg, ("wraptrue", "colour-{}".format(colour)))
            else:
                if re.search("https?:.+", msg):
                    self.hyperlink(msg, None)
                else:
                    root.chatbox.insert(END, msg, "wraptrue")
            root.chatbox.config(state = DISABLED)
            root.chatbox.see(END)
            if not root.focus:
                if "notificon.ico":
                    root.master.iconbitmap("notificon.ico")

    def hyperlink(self, msg, colour):
        text = re.split("https?://[A-Za-z0-9./_-]+[A-Za-z0-9/]", msg)
        links = re.findall("https?://[A-Za-z0-9./_-]+[A-Za-z0-9/]", msg)

        for i in range(max(len(text), len(links))):
            try:
                if root.showcolours and colour:
                    root.chatbox.insert(END, text[i], ("wraptrue", "colour-{}".format(colour)))
                else:
                    root.chatbox.insert(END, text[i], "wraptrue")
            except:
                pass
            try:
                link = links[i]
                action = lambda: webbrowser.open(link)
                root.chatbox.insert(END, link, root.hyperlinkmanager.add(action))
            except:
                pass

    def sendMsg(self, msg):
        txt = (msg + "\r\n").encode()
        self.transport.write(txt)

    def connectionMade(self):
        root.chatbox.config(state = NORMAL)
        root.chatbox.delete(1.0, END)
        root.chatbox.config(state = DISABLED)
        self.transport.write((root.nickname + "\r\n").encode())
        print("connected")

    def connectionLost(self, reason = connectionDone):
        print("connection lost")
        root.chatbox.config(state = NORMAL)
        root.chatbox.insert(END, "CONNECTION LOST")
        root.chatbox.config(state = DISABLED)
        root.chatbox.see(END)


class ChatFactory(ClientFactory):
    protocol = Client

    def buildProtocol(self, addr):
        self.instance = self.protocol()
        return self.instance


class ConnThread(object):
    def __init__(self, master):
        # connection threads
        self.connthread = threading.Thread(target = master.connection)


class App(Frame):
    def __init__(self, master = None):
        self.master = master

        # default style settings
        self.bg = "#000000"
        self.fontcol = "#ffffff"
        self.fontface = font.Font(family = "Lucida Console", size = 8)
        self.showcolours = True
        self.chatcolours = []
        self.icon = "plainicon.ico"

        # main frame
        Frame.__init__(self, master, background = self.bg)
        self.grid(sticky = N + E + S + W, padx = 10, pady = 10)

        # configuring the master Tk()
        self.master.grid_rowconfigure(0, weight = 1, minsize = 100)
        self.master.grid_columnconfigure(0, weight = 1, minsize = 100)

        self.master.geometry("300x200")
        self.master.title("firestarter")
        if self.icon:
            self.master.iconbitmap(self.icon)

        # enabling icon notifications
        self.focus = True
        self.master.bind('<FocusOut>', self.focusoff)
        self.master.bind('<FocusIn>', self.focuson)
        self.state = "disconnected"
        self.compname = socket.gethostname()

        # chat box
        self.chatbox = Text(self)
        self.chatbox.config(state = NORMAL)
        self.chatbox.tag_config("wraptrue", wrap = WORD)
        self.chatbox.insert(END, "Invalid host/port. Please disconnect and try again.")
        self.chatbox.config(state = DISABLED)
        self.hyperlinkmanager = tkhyperlink.HyperlinkManager(self.chatbox)

        # entry box and send button
        self.entryframe = Frame(self)
        self.entrybox = Entry(self.entryframe)
        self.addmenuicon = PhotoImage(file = "arrow.gif")
        self.deletemenuicon = PhotoImage(file = "arrowdown.gif")
        self.addmenubtn = Button(self.entryframe, command = self.addmenu)
        self.deletemenubtn = Button(self.entryframe, command = self.deletemenu)

        # initial connection widgets
        self.hostportframe = Frame(self, bg = self.bg)
        self.hostlab = Label(self.hostportframe, text = "Host:")
        self.hostlab.config(bg = self.bg, fg = self.fontcol)
        self.hostbox = Entry(self.hostportframe)
        self.hostbox.config(bg = self.bg, highlightthickness = 1, highlightcolor = self.fontcol, bd = 0,
                            fg = self.fontcol,
                            justify = CENTER)
        self.portlab = Label(self.hostportframe, text = "Port:")
        self.portlab.config(bg = self.bg, fg = self.fontcol)
        self.portbox = Entry(self.hostportframe)
        self.portbox.config(bg = self.bg, highlightthickness = 1, highlightcolor = self.fontcol, bd = 0,
                            fg = self.fontcol,
                            justify = CENTER)

        self.nicklab = Label(self, text = "Choose Nickname:")
        self.nicklab.config(bg = self.bg, fg = self.fontcol)
        self.nickbox = Entry(self)
        self.nickbox.config(bg = self.bg, highlightthickness = 1, highlightcolor = self.fontcol, bd = 0,
                            fg = self.fontcol,
                            justify = CENTER)
        self.nickbox.insert(END, self.compname)
        self.connbtn = Button(self, text = "Connect", command = self.startconnect)
        self.connbtn.config(bg = self.bg, highlightthickness = 0, bd = 0, fg = self.fontcol,
                            activebackground = self.bg)

        self.f = ChatFactory()

        self.connectlayout()

        self.colourwidgets()

        self.master.bind('<F1>', self.emergency)

    def colourwidgets(self):
        self.master.configure(background = self.bg)
        self.entryframe.config(bg = self.bg)
        self.chatbox.config(bg = self.bg, highlightthickness = 0, bd = 0, fg = self.fontcol, font = self.fontface)
        self.entrybox.config(bg = self.bg, highlightthickness = 1, highlightcolor = self.fontcol, bd = 0,
                             fg = self.fontcol)
        self.addmenubtn.config(bg = self.bg, highlightthickness = 0, bd = 0, fg = self.fontcol,
                               activebackground = self.bg, image = self.addmenuicon)
        self.deletemenubtn.config(bg = self.bg, highlightthickness = 0, bd = 0, fg = self.fontcol,
                                  activebackground = self.bg, image = self.deletemenuicon)

    def focusoff(self, event):
        self.focus = False

    def focuson(self, event):
        self.focus = True
        if "plainicon.ico":
            self.master.iconbitmap("plainicon.ico")

    def emergency(self, event):
        print(event)
        os.startfile("c:\windows\system32\cmd.exe")
        self.master.iconify()

    def connectlayout(self):
        self.hostportframe.grid(row = 0, column = 0, sticky = E + W + N + S)
        self.hostlab.grid(row = 0, column = 0, sticky = E + W, padx = 5)
        self.hostbox.grid(row = 1, column = 0, sticky = E + W, padx = 5)
        self.portlab.grid(row = 0, column = 1, sticky = E + W, padx = 5)
        self.portbox.grid(row = 1, column = 1, sticky = E + W, padx = 5)
        self.nicklab.grid(row = 1, column = 0, sticky = E + W + S, pady = 5)
        self.nickbox.grid(row = 2, column = 0, sticky = E + W)
        self.connbtn.grid(row = 3, column = 0, sticky = N, pady = 20)
        for i in range(self.hostportframe.grid_size()[0]):
            self.hostportframe.grid_columnconfigure(i, weight = 1)
        self.grid_columnconfigure(0, weight = 1)
        self.grid_rowconfigure(1, weight = 1)
        self.grid_rowconfigure(3, weight = 1)

    def layout(self):
        self.chatbox.grid(row = 0, column = 0, sticky = N + S + E + W)
        self.entryframe.grid(row = 1, column = 0, sticky = N + S + E + W)
        self.entrybox.grid(row = 0, column = 0, sticky = N + S + E + W)
        self.addmenubtn.grid(row = 0, column = 1, padx = (10, 0))

    def addmenu(self):
        # main menubar
        self.menubar = Menu(self)
        self.menubar.config(bg = self.bg, fg = self.fontcol)

        # style menubar
        self.stylemenu = Menu(self.menubar)
        self.stylemenu.config(bg = self.bg, fg = self.fontcol)

        # adding commands to the main menubar
        self.menubar.add_command(label = "Disconnect", background = self.bg, command = self.disconnect)
        self.menubar.add_command(label = "About", background = self.bg, command = self.printabout)
        issuelink = lambda: webbrowser.open("https://bitbucket.org/alicejenny/firestarter/issues/new")
        self.menubar.add_command(label = "Requests/Bugs", background = self.bg, command = issuelink)
        newcodewindow = lambda: codepaste.CodeInsert(self.bg, self.fontcol, self.fontface, self.icon, prevcode = None,
                                                     factory = self.f, master = self)
        self.menubar.add_command(label = "Code", background = self.bg, command = newcodewindow)
        self.menubar.add_cascade(label = "Style", background = self.bg, menu = self.stylemenu)

        def toggle():
            self.showcolours = not self.showcolours

        self.menubar.add_command(label = "Toggle Colours", background = self.bg, command = lambda: toggle())

        # adding commands to the style menubar
        self.stylemenu.add_command(label = "Colours", background = self.bg, command = self.colourpopup)

        # adding them to the window
        self.master.config(menu = self.menubar)

        # switching the icon to "collapse menu"
        self.addmenubtn.grid_forget()
        self.deletemenubtn.grid(row = 0, column = 1, padx = (10, 0))

        # starting a thread that deletes the menu after 5 seconds
        def sleepf():
            time.sleep(5)
            try:
                self.deletemenu()
            except:
                pass

        sleepthread = threading.Thread(target = sleepf, daemon = True)
        sleepthread.start()

    def deletemenu(self):
        self.menubar.destroy()
        self.deletemenubtn.grid_forget()
        self.addmenubtn.grid(row = 0, column = 1, padx = (10, 0))

    def colourpopup(self):
        self.popup = Toplevel()
        self.popup.title("")
        self.popup.config(bg = self.bg)
        if self.icon:
            self.popup.iconbitmap(self.icon)

        # background
        self.bglab = Label(self.popup, text = "Background:")
        self.bglab.config(bg = self.bg, fg = self.fontcol)
        self.bglab.grid(row = 0, column = 0)
        self.bgbox = Entry(self.popup)
        self.bgbox.config(bg = self.bg, highlightthickness = 1, highlightcolor = self.fontcol, bd = 0,
                          fg = self.fontcol, justify = CENTER)
        self.bgbox.grid(row = 0, column = 1)
        self.bgbox.insert(0, self.bg)
        self.bgbox.bind('<FocusOut>', self.changebg)

        # font colour
        self.fontcollab = Label(self.popup, text = "Text:")
        self.fontcollab.config(bg = self.bg, fg = self.fontcol)
        self.fontcollab.grid(row = 1, column = 0)
        self.fontcolbox = Entry(self.popup)
        self.fontcolbox.config(bg = self.bg, highlightthickness = 1, highlightcolor = self.fontcol, bd = 0,
                               fg = self.fontcol, justify = CENTER)
        self.fontcolbox.grid(row = 1, column = 1)
        self.fontcolbox.insert(0, self.fontcol)
        self.fontcolbox.bind('<FocusOut>', self.changefontcol)

    def changebg(self, event):
        try:
            self.bg = self.bgbox.get()
            self.colourwidgets()
        except Exception as e:
            pass

    def changefontcol(self, event):
        try:
            self.fontcol = self.fontcolbox.get()
            self.colourwidgets()
        except Exception as e:
            pass

    def expandable(self):
        for i in range(self.grid_size()[0]):
            self.grid_columnconfigure(i, weight = 1, minsize = 50)
        self.grid_rowconfigure(0, weight = 1, minsize = 50)
        self.entryframe.grid_columnconfigure(0, weight = 1, minsize = 50)

    def connection(self):
        try:
            self.state = "connected"
            reactor.connectTCP(self.host, int(self.port), self.f)
            while self.state == "connected":
                reactor.run(installSignalHandlers = 0)
        except:
            self.state = "disconnected"

    def startconnect(self):
        self.nickname = self.nickbox.get()
        self.host = self.hostbox.get()
        self.port = self.portbox.get()
        if not self.nickname or not self.host or not self.port:
            return
        newthread = ConnThread(self)
        newthread.connthread.start()
        print(self.state)
        if self.state == "connected":
            # runs a function to place the widgets (and remove the old ones)
            self.hostportframe.grid_forget()
            self.nicklab.grid_forget()
            self.nickbox.grid_forget()
            self.connbtn.grid_forget()
            for i in range(self.hostportframe.grid_size()[0]):
                self.grid_columnconfigure(i, weight = 0)
            self.grid_columnconfigure(0, weight = 0)
            self.grid_rowconfigure(1, weight = 0)
            self.grid_rowconfigure(3, weight = 0)
            self.layout()
            # runs a function to make widgets expandable
            self.expandable()

            # bind the enter key
            self.entrybox.bind('<Return>', self.sendmsg)
        else:
            del newthread
            return

    def sendmsg(self, event):
        msg = self.entrybox.get()
        if msg == "/howlongleft":
            end = dt.strptime(str(dt.today().date()) + " 17:30:00", "%Y-%m-%d %H:%M:%S")
            if dt.now() > end:
                rem = dt.now() - end
                msg = "work finished " + str(rem) + " ago"
            else:
                rem = end - dt.now()
                msg = "you've got " + str(rem) + " left"
        self.f.instance.sendMsg(msg)
        self.entrybox.delete(0, 'end')

    def disconnect(self):
        try:
            reactor.stop()
        except:
            pass
        sys.exit()

    def printabout(self):
        self.chatbox.config(state = NORMAL)
        self.chatbox.insert(END, "\n\nfirestarter version " + str(version) + "\n", "wraptrue")
        self.chatbox.insert(END, "last updated on " + str(updater.lastupdated("firestarter.exe")) + "\n\n", "wraptrue")
        self.chatbox.insert(END, "changes in the last update:\n- " + "\n- ".join(changes) + "\n\n", "wraptrue")
        self.chatbox.config(state = DISABLED)
        self.chatbox.see(END)


base = Tk()
root = App(base)
root.mainloop()
try:
    reactor.stop()
except:
    pass
print("disconnected")

try:
    updater.update("alicejenny", "firestarter")
except Exception as e:
    updater.writetolog(str(e))
