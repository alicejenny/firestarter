from tkinter import *

class CodeInsert(Toplevel):
    def __init__(self, bgcol, fontcol, fontface, icon, factory, prevcode = None, master = None):
        super().__init__(master)
        self.bg = bgcol
        self.fg = fontcol
        self.font = fontface
        self.title = "Paste Code"
        self.f = factory
        self.prevcode = prevcode
        self.config(bg = self.bg)
        if icon:
            self.iconbitmap(icon)
        self.widgets()

    def widgets(self):
        self.codebox = Text(self)
        self.codebox.config(bg = self.bg, highlightthickness = 1, bd = 0, fg = self.fg, font = self.font)
        self.codebox.grid(row = 0, column = 0, sticky = N + S + E + W)
        if self.prevcode:
            self.codebox.insert(END, self.prevcode)

        self.sendbtn = Button(self, text = "Send", command = self.sendcode)
        self.sendbtn.config(bg = self.bg, highlightthickness = 0, bd = 0, fg = self.fg,
                            activebackground = self.bg)
        self.sendbtn.grid(row = 1, column = 0, sticky = N + S + E + W)

        for i in range(self.grid_size()[0]):
            self.grid_columnconfigure(i, weight = 1, minsize = 50)
        self.grid_rowconfigure(0, weight = 1, minsize = 50)

    def sendcode(self):
        code = "[BEGIN CODE]" + self.codebox.get("1.0", "end-1c")
        self.f.instance.sendMsg(code)
        self.destroy()


class CodeView(Toplevel):
    def __init__(self, bgcol, fontcol, fontface, icon, code, factory, master):
        super().__init__(master)
        self.bg = bgcol
        self.fg = fontcol
        self.font = fontface
        self.title = "View Code"
        self.code = code
        self.f = factory
        self.icon = icon
        self.config(bg = self.bg)
        if self.icon:
            self.iconbitmap(self.icon)
        self.widgets()

    def widgets(self):
        self.codebox = Text(self)
        self.codebox.grid(row = 0, column = 0, sticky = N + S + E + W)
        self.codebox.config(bg = self.bg, highlightthickness = 0, bd = 0, fg = self.fg, font = self.font)
        self.codebox.insert(END, self.code)
        self.codebox.config(state = DISABLED)

        self.editbtn = Button(self, text = "Edit", command = self.editcode)
        self.editbtn.config(bg = self.bg, highlightthickness = 1, bd = 0, fg = self.fg,
                            activebackground = self.bg)
        self.editbtn.grid(row = 1, column = 0, sticky = N + S + E + W)

        for i in range(self.grid_size()[0]):
            self.grid_columnconfigure(i, weight = 1, minsize = 50)
        self.grid_rowconfigure(0, weight = 1, minsize = 50)

    def editcode(self):
        CodeInsert(self.bg, self.fg, self.font, self.icon, self.f, prevcode = self.codebox.get("1.0", "end-1c"), master = self.master)
        self.destroy()