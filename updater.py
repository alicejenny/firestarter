from bs4 import BeautifulSoup
import urllib.request
from datetime import datetime as dt
import os
from stat import *
import tkinter as tk
import re

class Item(object):
    def __init__(self, name, size, date, url):
        self.name = name
        self.size = size
        self.date = date
        self.url = url

    def __repr__(self):
        return "\n".join([self.name, str(self.size), str(self.date), self.url])

def update(user, project):
    bitbucket = "https://bitbucket.org"
    url = "/".join([bitbucket, user, project, "downloads"])

    with urllib.request.urlopen(url) as response:
        html = response.read()
    table = BeautifulSoup(html, "lxml")

    rows = table.find_all("tr")

    downloads = {}

    for row in rows:
        row = BeautifulSoup(str(row), "lxml")
        try:
            name = str(BeautifulSoup(str(row.find("td", "name")), "lxml").string)
            rawsize = str(BeautifulSoup(str(row.find("td", "size")), "lxml").string)
            rawdate = str(BeautifulSoup(str(row.find("time")["datetime"]), "lxml").string)
            url = bitbucket + str(BeautifulSoup(str(row.find("a")["href"]), "lxml").string)

            # convert size to bytes
            n = rawsize.split("\xa0")[0]
            unit = rawsize.split("\xa0")[1]
            if unit == "MB":
                n = int(float(n) * 1048576)
                unit = "bytes"
            elif unit == "KB":
                n = int(float(n) * 1024)
                unit = "bytes"
            elif unit == "bytes":
                n = int(n)
            else:
                pass

            # convert date to datetime object
            date = dt.strptime(rawdate, "%Y-%m-%dT%H:%M:%S.%f")

            if name and n and date and url:
                downloads[str(name)] = Item(name, n, date, url)
                # print(downloads[str(name)])
        except:
            pass

    currentfolder = os.getcwd()

    if "firestarterold.exe" in os.listdir(currentfolder):
        os.remove("firestarterold.exe")

    files = os.listdir(currentfolder)

    updated = 0
    new = 0

    # update files
    for file in files:
        if file in downloads.keys():
            online = downloads[file].date
            st = os.stat(file)
            local = dt.fromtimestamp(st[ST_MTIME])
            if online > local:
                try:
                    urllib.request.urlretrieve(downloads[file].url, file)
                    writetolog("Updated: " + file)
                    updated += 1
                except Exception as e:
                    writetolog(str(e))
    # get any new files
    for download in downloads:
        if download not in files:
            try:
                urllib.request.urlretrieve(downloads[download].url, download)
                writetolog("New file: " + download)
                new += 1
            except Exception as e:
                writetolog(str(e))

    if updated:
        msg = str(updated) + " files have been updated.\n"
    else:
        msg = ""
        writetolog("nothing to update")
    if new:
        msg += str(new) + " new files have been downloaded."
    else:
        writetolog("nothing to download")

    nowfiles = os.listdir(currentfolder)
    for file in nowfiles:
        if re.match(".+\.remove", file):
            os.rename("firestarter.exe", "firestarterold.exe")
            os.rename(file, re.split("\.remove", file)[0])

    if msg:
        msgbox = tk.Tk()
        updatemsg = tk.Label(msgbox, text = msg)
        updatemsg.grid(sticky = tk.N +tk.E + tk.S + tk.W)
        msgbox.title("updater")
        msgbox.geometry("200x50")
        msgbox.grid_columnconfigure(0, weight = 1)
        msgbox.mainloop()

def writetolog(msg):
    with open("log.txt", "a") as logfile:
        logfile.write(dt.strftime(dt.now(), "%H:%M:%S") + " " + msg + "\n")
        logfile.close()


def lastupdated(file):
    st = os.stat(file)
    return dt.fromtimestamp(st[ST_MTIME])