from twisted.internet import reactor
from twisted.internet.protocol import Factory, connectionDone
from twisted.protocols.basic import LineReceiver
import re
from datetime import datetime
import datetime as dt
import os
import codepaste


class Chatter(LineReceiver):
    def __init__(self, users, whois):
        self.users = users
        self.name = None
        self.state = "GETNAME"
        self.whois = whois
        self.status = "Joined but hasn't said anything"
        self.chatcolour = "#ffffff"

    def connectionMade(self):
        print("New connection made")

    def connectionLost(self, reason = connectionDone):
        if self.users:
            for user in self.users:
                self.users[user].sendLine("{} has just left.".format(self.name.decode()).encode())
        if self.name in self.users:
            del self.users[self.name]

    def lineReceived(self, line):
        if self.state == "GETNAME":
            self.handle_GETNAME(line)
        else:
            self.handle_CHAT(line)

    def handle_GETNAME(self, name):
        try:
            name = self.msgFormatting(name)
        except:
            name = "IChoseASillyName".encode()
        if name in self.users:
            self.sendLine("Someone's already using that one. Pick another:".encode())
            return
        elif not name.isalnum():
            self.sendLine("No special characters or backspaces. Pick another, or try again:".encode())
            return
        elif len(name) > 20:
            self.sendLine("That's way too long. Try again:".encode())
            return
        self.sendLine("Welcome, {}!".format(name.decode()).encode())
        if self.checkstatus():
            self.sendLine(self.checkstatus().encode())
        else:
            self.sendLine("You're the only one here!".encode())
        if self.users:
            for user in self.users:
                self.users[user].sendLine("\n\r{} has just joined.\n".format(name.decode()).encode())
        self.name = name
        self.users[name] = self
        self.state = "CHAT"
        self.whois[name] = "ur mum".encode()

    def handle_CHAT(self, message):
        try:
            if re.search("^\[BEGIN CODE\]", message.decode()):
                print("Code received")
                message = self.msgFormatting(message)
            else:
                message = self.msgFormatting(message[0:200])
            if re.search("^(/kick )(.+)$", message.decode()):
                user = re.search("(/kick )(.+)", message.decode()).groups()[1].encode()
                if user in list(self.users.keys()) and user != self.name:
                    self.users[user].transport.loseConnection()
                    del self.users[user]
                    self.sendToAll(
                        "{0} got kicked by {1}. Remember to only use this power for good!".format(user.decode(),
                                                                                                  self.name.decode()).encode())
                else:
                    self.sendLine("Maybe you should try that again.".encode())
            elif message.decode() == "/users":
                self.sendLine(self.printUsers())
            elif re.search("^(/pm )(.+)(: )(.+)$", message.decode()):
                user = re.search("(/pm )(.+)(: )(.+)", message.decode()).groups()[1].encode()
                message = re.search("(/pm )(.+)(: )(.+)", message.decode()).groups()[3]
                if self.users[user]:
                    recipient = self.users[user]
                    msg = "PRIVATE MESSAGE FROM {0}: {1}".format(self.name.decode(), message).encode()
                    recipient.sendLine(msg)
                    self.sendLine("PM sent.".encode())
            elif message.decode() == "/exit":
                self.transport.loseConnection()
            elif message.decode() == "/help":
                self.sendLine("\n\r".encode())
                with open("\\".join([os.getcwd(), "helptxt.txt"]), "r") as file:
                    helptxt = file.readlines()
                for line in helptxt:
                    self.sendLine(line.encode())
            elif message.decode() == "/status":
                self.sendLine(self.checkstatus().encode())
            elif re.search("^/chatcolour (.+)$", message.decode()):
                self.chatcolour = re.search("^/chatcolour (.+)$", message.decode()).groups()[0][0:7]
            elif re.search("^(/version )(.+)$", message.decode()):
                user = re.search("(/version )(.+)", message.decode()).groups()[1].encode()
                if self.users[user]:
                    recipient = self.users[user]
                    msg = "IF YOU CAN SEE THIS MESSAGE, YOU NEED TO UPDATE.\nhttp://bitbucket.com/alicejenny/firestarter/downloads".encode()
                    recipient.sendLine(msg)
            elif re.search("^(/silent )(.+)(: )(.+)$", message.decode()):
                user = re.search("(/silent )(.+)(: )(.+)", message.decode()).groups()[1].encode()
                message = re.search("(/silent )(.+)(: )(.+)", message.decode()).groups()[3]
                if self.users[user]:
                    recipient = self.users[user]
                    msg = "VERSION NUMBER FOR {0}: {1}".format(self.name.decode(), message).encode()
                    recipient.sendLine(msg)
            else:
                timestamp = datetime.strftime(datetime.now(), "%H:%M:%S")
                message = "[{0}]({1}) {2}: {3}".format(self.chatcolour, str(timestamp), self.name.decode(), message.decode())
                self.sendToAll(message.encode())
                self.lastmessage = datetime.now()
        except Exception as e:
            self.sendLine("STOP BREAKING IT".encode())
            print(e)

    def sendToAll(self, message):
        for name in self.users:
            protocol = self.users[name]
            protocol.sendLine(message)

    def printUsers(self):
        nicklist = ["You"]
        for user in self.users:
            if user == self.name:
                pass
            else:
                nicklist.append(user.decode())
        if len(nicklist) == 1:
            return "Just you, {}.".format(self.name.decode()).encode()
        else:
            herelist = ", ".join(nicklist[0:len(nicklist) - 1]) \
                       + " and " \
                       + nicklist[len(nicklist) - 1] \
                       + "."
            return herelist.encode()

    def msgFormatting(self, message):
        try:
            print(self.name.decode(), message)
        except:
            print(message)
        decoded = message.decode('ascii', 'ignore')
        if re.search("\\x08", decoded):
            if re.search("^(\\x08+)(.*)", decoded):
                decoded = re.search("^(\\x08+)(.*)", decoded).groups()[1]
            if not re.search("\\x08", decoded):
                return decoded.encode()
            allmatches = re.findall("([^(\\x08)]+)(\\x08+)", decoded)

            totalmsg = ""
            for group in allmatches:
                nb = len(group[1])
                rawmsg = group[0]
                if nb > len(rawmsg):
                    newmsg = ""
                else:
                    newmsg = rawmsg[0:len(rawmsg) - nb]
                totalmsg += newmsg
            if not re.search("(.*)(\\x08+)$", decoded):
                end = re.search("(\\x08+)(.+)", decoded).groups()[1]
                totalmsg += end

            return totalmsg.encode()
        else:
            return message.decode('ascii', 'ignore').encode()

    def checkstatus(self):
        listmsg = []
        for name in self.users:
            try:
                lastactive = datetime.now() - self.users[name].lastmessage
            except:
                lastactive = 0
            if lastactive and lastactive > dt.timedelta(minutes = 30):
                self.users[name].status = "Away"
            elif lastactive and lastactive < dt.timedelta(minutes = 30):
                self.users[name].status = "Active"
            if self.users[name].status == "Active":
                lastactivemsg = "Last message was sent " + str(lastactive) + " ago."
                listmsg.append(name.decode() + ": " + self.users[name].status + ". " + lastactivemsg)
            else:
                listmsg.append(name.decode() + ": " + self.users[name].status)
        return ("\n\r".join(listmsg))


class ChatFactory(Factory):
    def __init__(self):
        self.users = {}
        self.whois = {}

    def buildProtocol(self, addr):
        return Chatter(self.users, self.whois)


reactor.listenTCP(int(input("Choose port: ")), ChatFactory())
print("Listening...")
reactor.run()
